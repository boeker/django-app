from django.db import models


class Book(models.Model):
    id = models.BigIntegerField(primary_key=True)
    self_id = models.TextField()
    user = models.TextField()
    tags = models.TextField()
    title = models.TextField()
    authors = models.TextField()
    publisher = models.CharField(max_length=200)
    pub_date = models.DateField()
    description = models.TextField()
    page_count = models.IntegerField()
    categories = models.TextField()
    language = models.CharField(max_length=2)
    image_link = models.TextField()

    def __str__(self):
        return f'{self.title}|{self.authors}|{self.publisher}|{self.pub_date}|{self.description}|' \
               f'{self.page_count}|{self.categories}|{self.language}|{self.image_link}|{self.self_id}|' \
               f'{self.user}|{self.tags}'
