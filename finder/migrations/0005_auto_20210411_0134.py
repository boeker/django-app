# Generated by Django 3.1.6 on 2021-04-10 18:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('finder', '0004_auto_20210411_0130'),
    ]

    operations = [
        migrations.RenameField(
            model_name='book',
            old_name='elf_id',
            new_name='self_id',
        ),
        migrations.AlterField(
            model_name='book',
            name='id',
            field=models.AutoField(primary_key=True, serialize=False),
        ),
    ]
