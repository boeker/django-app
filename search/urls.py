from django.urls import path
from . import views
from lists import urls

urlpatterns = [
    path('', views.index, name='home'),
    path('search', views.search, name='search'),
    path('book', views.book, name='book'),
    path('add_book', views.add_book, name='add_book'),
    path('delete_book', views.delete_book, name='delete_book'),
    path('library', views.library_books, name='library'),
    path('library_search', views.library_search, name='library_search'),
]
for i in urls.urlpatterns:
    urlpatterns.append(i)
