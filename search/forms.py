from .models import library_book
from django.forms import ModelForm


class LibraryBookForm(ModelForm):
    class Meta:
        model = library_book
        fields = ['title', 'authors', 'description', 'categories', 'language', 'page_count', 'pub_date', 'publisher', 'image_link', 'self_id', 'user', 'tags']
