from django.shortcuts import render
from finder.api import search_books
from lists.views import gen_avatar_url
from .models import library_book
from .forms import LibraryBookForm


def sort_function(sorting_values, books):
    for i in range(len(sorting_values)):
        cursor = sorting_values[i]
        book_cursor = books[i]
        pos = i
        while pos > 0 and sorting_values[pos - 1] > cursor:
            sorting_values[pos] = sorting_values[pos - 1]
            books[pos] = books[pos - 1]
            pos = pos - 1
        sorting_values[pos] = cursor
        books[pos] = book_cursor
    return books


def index(request):
    return render(request, 'search/search.html', {'avatar_url': gen_avatar_url(request.user),})


def search(request):
    query = request.GET['title']
    prompt = query
    title = request.GET['title']
    author = request.GET['author']
    category = request.GET['category']
    publisher = request.GET['publisher']
    pub_year = request.GET['pub_year']
    page_count_min = request.GET['page_count_min']
    page_count_max = request.GET['page_count_max']
    language = request.GET['language']
    user = request.GET['user']
    sort_type = request.GET['sort_type']
    sort_by = request.GET['sort_by']
    library_books = library_book.objects.all()
    if not query == '' or author or category or pub_year or page_count_min or \
                page_count_max or language:
        if not title and not author and not category and not pub_year:
            list_of_books = []
        else:
            title = f"intitle:{title}" if title else title
            author = f"inauthor:{author}" if author else author
            category = f"subject:{category}" if category else category
            publisher = f"inpublisher:{publisher}" if publisher else publisher
            prompt = query
            query = "+".join([title, author, publisher, category, pub_year])
            list_of_books = search_books(query)
            if pub_year:
                list_of_books = [i for i in list_of_books if i.pub_date and i.pub_date.year == int(pub_year)]
            if page_count_min:
                list_of_books = [i for i in list_of_books if i.page_count and i.page_count >= int(page_count_min)]
            if page_count_max:
                list_of_books = [i for i in list_of_books if i.page_count and i.page_count <= int(page_count_max)]
            if language:
                list_of_books = [i for i in list_of_books if i.language and i.language == language]
            if category[:8] == 'subject:':
                category = category[8:]
            if author[:9] == 'inauthor:':
                author = author[9:]
            if publisher[:12] == 'inpublisher:':
                publisher = publisher[12:]
        for book in list_of_books:
            if book.title == 'None':
                book.title = None
            if book.authors == 'None':
                book.authors = None
            if book.publisher == 'None':
                book.publisher = None
            if book.pub_date == 'None':
                book.pub_date = None
            if book.description == 'None':
                book.description = None
            if book.description and len(book.description) > 550:
                length = (len(book.description) - 550) * -1
                space = book.description.find(' ', length)
                book.description_short = book.description[:space] + '...'
            if book.page_count == 'None':
                book.page_count = None
            if book.categories == 'None':
                book.categories = None
            if book.language == 'None':
                book.language = None
            if book.image_link is None:
                book.image_link = 'https://media.istockphoto.com/photos/book-blank-book-with-white-cover-picture' \
                                  '-id494902904?s=170x170 '
            for lib_book in library_books:
                if book.self_id == lib_book.self_id and lib_book.user == user:
                    book.added = True
                    if lib_book.tags:
                        book.tags = lib_book.tags

    else:
        list_of_books = []

    if list_of_books and sort_type and sort_by:
        no_sorting_list = []
        sorting_list = list_of_books
        sorting_values = []

        if sort_type == 'year':
            no_sorting_list = [book for book in list_of_books if not book.pub_date]
            sorting_list = [book for book in list_of_books if book.pub_date]
            if sort_by == 'ascend':
                sorting_values = [book.pub_date.year for book in sorting_list]
            if sort_by == 'descend':
                sorting_values = [-1 * book.pub_date.year for book in sorting_list]

        if sort_type == 'size':
            no_sorting_list = [book for book in list_of_books if not book.page_count]
            sorting_list = [book for book in list_of_books if book.page_count]
            if sort_by == 'ascend':
                sorting_values = [book.page_count for book in sorting_list]
            if sort_by == 'descend':
                sorting_values = [-1 * book.page_count for book in sorting_list]

        sorting_list = sort_function(sorting_values, sorting_list)
        list_of_books = sorting_list + no_sorting_list

    context = {
        'list': list_of_books,
        'query': prompt,
        'author': author,
        'publisher': publisher,
        'category': category,
        'pub_year': pub_year,
        'page_count_min': page_count_min,
        'page_count_max': page_count_max,
        'language': language,
        'empty': 'Поиск',
        'avatar_url': gen_avatar_url(request.user),
    }
    return render(request, 'search/search_result.html', context)


def library_search(request):
    title = request.GET['title']
    author = request.GET['author']
    category = request.GET['category']
    publisher = request.GET['publisher']
    pub_year = request.GET['pub_year']
    page_count_min = request.GET['page_count_min']
    page_count_max = request.GET['page_count_max']
    language = request.GET['language']
    tags = request.GET['tags']
    user = request.GET['user']
    sort_type = request.GET['sort_type']
    sort_by = request.GET['sort_by']
    if tags and '#' not in tags:
        tags = '#' + tags
    search_tags = tags.split('#')
    search_tags.pop(0)
    for i in range(0, len(search_tags)):
        if (search_tags[i])[-1] == ' ':
            search_tags[i] = (search_tags[i])[0:-1]
    if not title and not author and not category and not pub_year and not tags:
        books = []
    else:
        books = []
        if len(search_tags) > 1:
            tmp_books = []
            counter = 0
            for tag in search_tags:
                if counter != 0:
                    compare_books = library_book.objects.filter(title__icontains=title, authors__icontains=author, categories__icontains=category,
                                                        publisher__icontains=publisher, pub_date__icontains=pub_year, language__icontains=language,
                                                        tags__icontains=tag, user=user)
                    for book in compare_books:
                        book.delete_condition = 0
                    for book in compare_books:
                        if book in tmp_books:
                            if not book.delete_condition:
                                book.delete_condition = 0
                            if book not in books:
                                books.append(book)
                        else:
                            book.delete_condition = 1
                            if book not in books:
                                books.append(book)
                            else:
                                books[books.index(book)].delete_condition = 1
                    for book in tmp_books:
                        if book in compare_books:
                            if not book.delete_condition:
                                book.delete_condition = 0
                            if book not in books:
                                books.append(book)
                        else:
                            book.delete_condition = 1
                            if book not in books:
                                books.append(book)
                            else:
                                books[books.index(book)].delete_condition = 1
                else:
                    tmp_books = library_book.objects.filter(title__icontains=title, authors__icontains=author, categories__icontains=category,
                                                        publisher__icontains=publisher, pub_date__icontains=pub_year, language__icontains=language,
                                                        tags__icontains=tag, user=user)
                    for book in tmp_books:
                        book.delete_condition = 0
                counter += 1
            for book in books:
                if book.delete_condition:
                    if book in books:
                        books.remove(book)
        else:
            if search_tags:
                books = library_book.objects.filter(title__icontains=title, authors__icontains=author,
                                                        categories__icontains=category,
                                                        publisher__icontains=publisher, pub_date__icontains=pub_year,
                                                        language__icontains=language,
                                                        tags__icontains=search_tags[0], user=user)
            else:
                books = library_book.objects.filter(title__icontains=title, authors__icontains=author,
                                                    categories__icontains=category,
                                                    publisher__icontains=publisher, pub_date__icontains=pub_year,
                                                    language__icontains=language, user=user)

    if page_count_min:
        books = [i for i in books if i.page_count and i.page_count >= int(page_count_min)]
    if page_count_max:
        books = [i for i in books if i.page_count and i.page_count <= int(page_count_max)]
    if not title and not author and not category and not publisher and not pub_year and not page_count_min and not page_count_max and not language and not tags:
        books = library_book.objects.filter(user=user)

    if books and sort_type and sort_by:
        no_sorting_list = []
        sorting_list = [book for book in books]
        sorting_values = []

        if sort_type == 'year':
            no_sorting_list = [book for book in books if not book.pub_date]
            sorting_list = [book for book in books if book.pub_date]
            if sort_by == 'ascend':
                sorting_values = [book.pub_date.year for book in sorting_list]
            if sort_by == 'descend':
                sorting_values = [-1*book.pub_date.year for book in sorting_list]

        if sort_type == 'size':
            no_sorting_list = [book for book in books if not book.page_count]
            sorting_list = [book for book in books if book.page_count]
            if sort_by == 'ascend':
                sorting_values = [book.page_count for book in sorting_list]
            if sort_by == 'descend':
                sorting_values = [-1*book.page_count for book in sorting_list]

        sorting_list = sort_function(sorting_values, sorting_list)
        books = sorting_list + no_sorting_list

    if books:
        for book in books:
            if book.description and len(book.description) > 550:
                length = (len(book.description) - 550) * -1
                space = book.description.find(' ', length)
                book.description_short = book.description[:space] + '...'
    if tags == '#не указаны':
        tags = tags[1:]
    return render(request, 'lists/library.html', {'books': books, 'query': title, 'author': author, 'category': category,
                                                  'empty': 'По вашему запросу в библиотеке ничего не нашлось', 'publisher': publisher,
                                                  'pub_year': pub_year, 'page_count_min': page_count_min, 'page_count_max': page_count_max,
                                                  'language': language, 'tags': tags, 'avatar_url': gen_avatar_url(request.user),})


def book(request):
    query = request.GET['s']
    user = request.GET['user']
    book_data = query.split('|')
    for i in range(len(book_data)):
        if book_data[i] == 'None':
            book_data[i] = None
    books = library_book.objects.all()
    condition = True
    for i in books:
        if i.self_id == book_data[9] and i.user == user:
            condition = None
    context = {
        'book': book_data,
        'title': book_data[0],
        'authors': book_data[1],
        'publisher': book_data[2],
        'pub_date': book_data[3],
        'description': book_data[4],
        'page_count': book_data[5],
        'categories': book_data[6],
        'language': book_data[7],
        'image_link': book_data[8],
        'self_id': book_data[9],
        'tags': book_data[11],
        'query': book_data[12],
        'author': book_data[13],
        'category': book_data[14],
        'empty': book_data[15],
        'publisher_search': book_data[16],
        'pub_year': book_data[17],
        'page_count_min': book_data[18],
        'page_count_max': book_data[19],
        'language_search': book_data[20],
        'tags_search': book_data[21],
        'condition': condition,
        'avatar_url': gen_avatar_url(request.user),
    }
    return render(request, 'search/book.html', context)


def library_books(request):
    user = request.POST['username']
    books = library_book.objects.filter(user=user)
    for book in books:
        if book.description and len(book.description) > 550:
            length = (len(book.description) - 550) * -1
            space = book.description.find(' ', length)
            book.description_short = book.description[:space] + '...'
    return render(request, 'lists/library.html', {'books': books, 'empty': 'В вашей библиотеке пока нет добавленных '
                                                                           'книг', 'avatar_url': gen_avatar_url(request.user),})


def add_book(request):
    book_data = request.POST['s']
    tags = request.POST['tags']
    user = request.POST['user']
    if not tags:
        tags = 'не указаны'
    if '#' not in tags and tags != 'не указаны':
        tags = '#' + tags
    if tags != 'не указаны':
        tags = tags.split('#')
        tags.pop(0)
        tags_line = ''
        tags = set(tags)
        tags = list(tags)
        for i in tags:
            tags_line += '#' + i + ' '
        tags = tags_line
    book_data = book_data.replace('None', '\'None\'')
    book_data = book_data.replace(', \"', ', \'')
    book_data = book_data.replace('\",', '\',')
    book_data = book_data.replace(']\"', '')
    book_data = book_data.replace('\"]', '\', \'')
    book_data = book_data.replace('\']', '\', \'')
    book_data = book_data.replace('\"[', '')
    book_data = book_data.replace('[\"', '')
    book_data = book_data.replace('[\'', '').replace('\']', '')
    book_data = book_data.split('\', \'')
    for i in range(len(book_data)):
        book_data[i] = book_data[i].replace('[\'', '').replace('\']', '')
    show_book = []
    for i in range(len(book_data)):
        show_book.append(book_data[i])
        if book_data[i] == 'None':
            show_book[i] = None

    for i in range(len(book_data)):
        if book_data[i] == 'None':
            if i == 5:
                book_data[i] = 0
            elif i == 1 or i == 2 or i >= 5 or i <= 7:
                book_data[i] = 'не указан'
            elif i == 3:
                book_data[i] = 'не указана'
            else:
                book_data[i] = 'не указано'

    book = {
        'title': book_data[0],
        'authors': book_data[1],
        'publisher': str(book_data[2]),
        'pub_date': book_data[3],
        'description': book_data[4],
        'page_count': str(book_data[5]),
        'categories': book_data[6],
        'language': str(book_data[7]),
        'image_link': book_data[8],
        'self_id': book_data[9],
        'user': book_data[22],
        'tags': tags,
        'avatar_url': gen_avatar_url(request.user),
    }
    query = LibraryBookForm(book)
    condition = False
    for i in library_book.objects.filter(user=user):
        condition = book['self_id'] == i.self_id
        if condition:
            break
    if query.is_valid() and not condition:
        query.save()
    title = f"intitle:{book_data[12]}" if book_data[12] else book_data[12]
    author = f"inauthor:{book_data[13]}" if book_data[13] else book_data[13]
    category = f"subject:{book_data[14]}" if book_data[14] else book_data[14]
    publisher = f"inpublisher:{book_data[16]}" if book_data[16] else book_data[16]
    pub_year = book_data[17]
    page_count_min = book_data[18]
    page_count_max = book_data[19]
    language = book_data[20]
    prompt = "+".join([title, author, publisher, category, pub_year])
    list_of_books = search_books(prompt)
    if pub_year:
        list_of_books = [i for i in list_of_books if i.pub_date and i.pub_date.year == int(pub_year)]
    if page_count_min:
        list_of_books = [i for i in list_of_books if i.page_count and i.page_count >= int(page_count_min)]
    if page_count_max:
        list_of_books = [i for i in list_of_books if i.page_count and i.page_count <= int(page_count_max)]
    if language:
        list_of_books = [i for i in list_of_books if i.language and i.language == language]
    library_books = library_book.objects.all()
    for book in list_of_books:
        if book.title == 'None':
            book.title = None
        if book.authors == 'None':
            book.authors = None
        if book.publisher == 'None':
            book.publisher = None
        if book.pub_date == 'None':
            book.pub_date = None
        if book.description == 'None':
            book.description = None
        if book.description and len(book.description) > 550:
            length = (len(book.description) - 550) * -1
            space = book.description.find(' ', length)
            book.description_short = book.description[:space] + '...'
        if book.page_count == 'None':
            book.page_count = None
        if book.categories == 'None':
            book.categories = None
        if book.language == 'None':
            book.language = None
        if book.image_link is None:
            book.image_link = 'https://media.istockphoto.com/photos/book-blank-book-with-white-cover-picture-id494902904?s=170x170'
        for lib_book in library_books:
            if book.self_id == lib_book.self_id and lib_book.user == user:
                book.added = True
                if lib_book.tags:
                    book.tags = lib_book.tags
    context = {
        'list': list_of_books,
        'query': book_data[12],
        'author': book_data[13],
        'category': book_data[14],
        'empty': book_data[15],
        'publisher': book_data[16],
        'pub_year': book_data[17],
        'page_count_min': book_data[18],
        'page_count_max': book_data[19],
        'language': book_data[20],
        'avatar_url': gen_avatar_url(request.user),
    }
    return render(request, 'search/search_result.html', context)


def delete_book(request):
    book_data = request.POST['s']
    user = request.POST['user']
    book_data = book_data.replace('None', '\'None\'')
    book_data = book_data.replace(', \"', ', \'')
    book_data = book_data.replace('\",', '\',')
    book_data = book_data.replace(']\"', '')
    book_data = book_data.replace('\"]', '\', \'')
    book_data = book_data.replace('\']', '\', \'')
    book_data = book_data.replace('\"[', '')
    book_data = book_data.replace('[\"', '')
    book_data = book_data.replace('[\'', '').replace('\']', '')
    book_data = book_data.split('|')
    library_book.objects.filter(self_id=book_data[0], user=user).delete()
    book_data[1] = book_data[1].replace('\\xa0', ' ')
    if book_data[4] == 'Поиск':
        href = 'search/search_result.html'
        title = f"intitle:{book_data[1]}" if book_data[1] else book_data[1]
        author = f"inauthor:{book_data[2]}" if book_data[2] else book_data[2]
        category = f"subject:{book_data[3]}" if book_data[3] else book_data[3]
        publisher = f"inpublisher:{book_data[5]}" if book_data[5] else book_data[5]
        pub_year = book_data[6]
        page_count_min = book_data[7]
        page_count_max = book_data[8]
        language = book_data[9]
        tags = book_data[10]
        prompt = "+".join([title, author, publisher, category, pub_year])
        list_of_books = search_books(prompt)
        if pub_year:
            list_of_books = [i for i in list_of_books if i.pub_date and i.pub_date.year == int(pub_year)]
        if page_count_min:
            list_of_books = [i for i in list_of_books if i.page_count and i.page_count >= int(page_count_min)]
        if page_count_max:
            list_of_books = [i for i in list_of_books if i.page_count and i.page_count <= int(page_count_max)]
        if language:
            list_of_books = [i for i in list_of_books if i.language and i.language == language]
        library_books = library_book.objects.all()
        empty = 'Поиск'
        for book in list_of_books:
            if book.title == 'None':
                book.title = None
            if book.authors == 'None':
                book.authors = None
            if book.publisher == 'None':
                book.publisher = None
            if book.pub_date == 'None':
                book.pub_date = None
            if book.description == 'None':
                book.description = None
            if book.description and len(book.description) > 550:
                length = (len(book.description) - 550) * -1
                space = book.description.find(' ', length)
                book.description_short = book.description[:space] + '...'
            if book.page_count == 'None':
                book.page_count = None
            if book.categories == 'None':
                book.categories = None
            if book.language == 'None':
                book.language = None
            if book.image_link is None:
                book.image_link = 'https://media.istockphoto.com/photos/book-blank-book-with-white-cover-picture-id494902904?s=170x170'
            for lib_book in library_books:
                if book.self_id == lib_book.self_id and lib_book.user == user:
                    book.added = True
                    if lib_book.tags:
                        book.tags = lib_book.tags
    else:
        href = 'lists/library.html'
        title = book_data[1]
        author = book_data[2]
        category = book_data[3]
        publisher = book_data[5]
        pub_year = book_data[6]
        page_count_min = book_data[7]
        page_count_max = book_data[8]
        language = book_data[9]
        tags = book_data[10]
        empty = 'В вашей библиотеке пока нет добавленных книг'
        search_tags = tags.split('#')
        search_tags.pop(0)
        for i in range(0, len(search_tags)):
            if (search_tags[i])[-1] == ' ':
                search_tags[i] = (search_tags[i])[0:-1]
        if not title and not author and not category and not pub_year and not tags:
            list_of_books = []
        else:
            list_of_books = []
            if len(search_tags) > 1:
                tmp_books = []
                counter = 0
                for tag in search_tags:
                    if counter != 0:
                        compare_books = library_book.objects.filter(title__icontains=title, authors__icontains=author,
                                                                    categories__icontains=category,
                                                                    publisher__icontains=publisher,
                                                                    pub_date__icontains=pub_year,
                                                                    language__icontains=language,
                                                                    tags__icontains=tag, user=user)
                        for book in compare_books:
                            book.delete_condition = 0
                        for book in compare_books:
                            if book in tmp_books:
                                if not book.delete_condition:
                                    book.delete_condition = 0
                                if book not in list_of_books:
                                    list_of_books.append(book)
                            else:
                                book.delete_condition = 1
                                if book not in list_of_books:
                                    list_of_books.append(book)
                                else:
                                    list_of_books[list_of_books.index(book)].delete_condition = 1
                        for book in tmp_books:
                            if book in compare_books:
                                if not book.delete_condition:
                                    book.delete_condition = 0
                                if book not in list_of_books:
                                    list_of_books.append(book)
                            else:
                                book.delete_condition = 1
                                if book not in list_of_books:
                                    list_of_books.append(book)
                                else:
                                    list_of_books[list_of_books.index(book)].delete_condition = 1
                    else:
                        tmp_books = library_book.objects.filter(title__icontains=title, authors__icontains=author,
                                                                categories__icontains=category,
                                                                publisher__icontains=publisher,
                                                                pub_date__icontains=pub_year,
                                                                language__icontains=language,
                                                                tags__icontains=tag, user=user)
                        for book in tmp_books:
                            book.delete_condition = 0
                    counter += 1
                for book in list_of_books:
                    if book.delete_condition:
                        if book in list_of_books:
                            list_of_books.remove(book)
            else:
                if search_tags:
                    list_of_books = library_book.objects.filter(title__icontains=title, authors__icontains=author,
                                                        categories__icontains=category,
                                                        publisher__icontains=publisher, pub_date__icontains=pub_year,
                                                        language__icontains=language,
                                                        tags__icontains=search_tags[0], user=user)
                else:
                    list_of_books = library_book.objects.filter(title__icontains=title, authors__icontains=author,
                                                        categories__icontains=category,
                                                        publisher__icontains=publisher, pub_date__icontains=pub_year,
                                                        language__icontains=language, user=user)
        if pub_year:
            list_of_books = [i for i in list_of_books if i.pub_date and i.pub_date.year == int(pub_year)]
        if page_count_min:
            list_of_books = [i for i in list_of_books if i.page_count and i.page_count >= int(page_count_min)]
        if page_count_max:
            list_of_books = [i for i in list_of_books if i.page_count and i.page_count <= int(page_count_max)]
        if language:
            list_of_books = [i for i in list_of_books if i.language and i.language == language]
        if list_of_books:
            for book in list_of_books:
                if book.description and len(book.description) > 550:
                    length = (len(book.description) - 550) * -1
                    space = book.description.find(' ', length)
                    book.description_short = book.description[:space] + '...'
        if title or author or category or pub_year or page_count_min or page_count_max or language or tags:
            empty = 'По вашему запросу в библиотеке ничего не нашлось'
        if not title and not author and not category and not publisher and not pub_year and not page_count_min and not page_count_max and not language and not tags:
            list_of_books = library_book.objects.filter(user=user)
    context = {
        'list': list_of_books,
        'books': list_of_books,
        'query': book_data[1],
        'author': book_data[2],
        'category': book_data[3],
        'publisher': book_data[5],
        'pub_year': book_data[6],
        'page_count_min': book_data[7],
        'page_count_max': book_data[8],
        'language': book_data[9],
        'tags': tags,
        'empty': empty,
        'avatar_url': gen_avatar_url(request.user),
    }
    return render(request, href, context)
