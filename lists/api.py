import requests
import datetime
from decouple import config
from .models import Book

API_KEY = config('API_KEY')
URL = f'https://www.googleapis.com/books/v1/volumes' \
      f'?key={API_KEY}&printType=books&maxResults=40'


def search_books(query):
    query.replace(' ', '+')
    response, books = requests.get(f'{URL}&q={query}').json(), []

    if response['totalItems'] > 0:
        for item in response['items']:
            info = item['volumeInfo']

            title = info.get('title')
            publisher = info.get('publisher')
            description = info.get('description')
            page_count = info.get('pageCount')
            language = info.get('language')

            authors = info.get('authors')
            authors = ', '.join(authors) if authors and len(authors) else None

            pub_date = info.get('publishedDate')
            if pub_date:
                date = [int(n) for n in pub_date.split('-')]
                while len(date) < 3:
                    date.append(1)
                pub_date = datetime.date(*tuple(date))

            categories = info.get('categories')
            if categories and len(categories):
                categories = ', '.join(categories)

            image_links, image_link = info.get('imageLinks'), None
            if image_links:
                image_link = image_links.get('thumbnail')

            books.append(Book(
                title=title,
                authors=authors,
                publisher=publisher,
                pub_date=pub_date,
                description=description,
                page_count=page_count,
                categories=categories,
                language=language,
                image_link=image_link
            ))
    return books
