from django.urls import path
from search import urls
from . import views

app_name = 'lists'
urlpatterns = [
    path('listing', views.listing, name='listing'),
    path('list', views.cur_list, name='list'),
    path('profile', views.profile, name='profile'),
    path('library', views.library, name='library'),
    path('password', views.change_password, name='password')
]
