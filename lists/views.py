from django.shortcuts import render, redirect
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.decorators import login_required
from .forms import UserUpdateForm
import hashlib

from .api import search_books


class List:
    def __init__(self, name, size, books=None):
        self.name = name
        self.size = size
        self.books = books


def listing(request):
    lists = []
    list1 = List("Добавленные", 4)
    lists.append(list1)
    if request.method == 'POST':
        query = request.POST
        try:
            name = query.__getitem__('list_name')
        except KeyError:
            pass
        else:
            list2 = List(name, 0)
            lists.append(list2)
    context = {
        'lists': tuple(lists),
    }
    return render(request, 'lists/lists.html', context)


def cur_list(request):
    books = search_books('None')[:4]
    list2 = List("Добавленные", 4, books)
    context = {
        'list': list2,
    }
    return render(request, 'lists/list.html', context)


def gen_avatar_url(user):
    if user.is_authenticated:
        email = user.email.strip().lower()
        avatar_hash, size = hashlib.md5(email.encode('utf-8')).hexdigest(), 512
        return f'https://gravatar.com/avatar/{avatar_hash}?s={size}'
    else:
        return


@login_required
def profile(request):
    if request.method == 'POST':
        form = UserUpdateForm(request.POST, instance=request.user)
        if form.is_valid():
            form.save()
            return redirect('profile')
    else:
        form = UserUpdateForm(instance=request.user)

    context = {
        'avatar_url': gen_avatar_url(request.user),
        'form': form,
    }
    return render(request, 'lists/profile.html', context)


@login_required
def change_password(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        form.fields['new_password1'].help_text = None
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)
            return redirect('profile')
    else:
        form = PasswordChangeForm(request.user)
        form.fields['new_password1'].help_text = None

    context = {
        'avatar_url': gen_avatar_url(request.user),
        'form': form,
    }
    return render(request, 'lists/change_password.html', context)


def library(request):
    books = search_books('The Remains of the Day')[:4]
    context = {
        'books': books,
    }
    return render(request, 'lists/library.html', context)
