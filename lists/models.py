from django.db import models


class Book(models.Model):
    title = models.TextField()
    authors = models.TextField()
    publisher = models.CharField(max_length=200)
    pub_date = models.DateField()
    description = models.TextField()
    page_count = models.IntegerField()
    categories = models.TextField()
    language = models.CharField(max_length=2)
    image_link = models.TextField()

    def __str__(self):
        return f'{self.title} ({self.pub_date}) - {self.page_count} pp.'
